/*
 Differential.js: JS HTML Animation/movement engine

 Author: Andrews54757
 License: MIT (https://github.com/ThreeLetters/differential.js/blob/master/LICENSE)
 Source: https://github.com/ThreeLetters/differential.js
 Build: v0.0.2
 Built on: 05/07/2018
*/

window.D = function (k) {
    function C(b) {
        return {
            easing: b.easing || "swing",
            done: b.done || function () {},
            queue: void 0 !== b.queue ? b.queue : !0,
            specialEasing: b.specialEasing,
            step: b.step,
            progress: b.progress,
            start: b.start || function () {},
            duration: b.duration || 400
        }
    }

    function P(b) {
        b = b.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function (c, a, b, g) {
            return a + a + b + b + g + g
        });
        return (b = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(b)) ? [parseInt(b[1], 16), parseInt(b[2], 16), parseInt(b[3], 16)] : null
    }

    function D(b, c, a, d) {
        function g(a, c,
            b) {
            return (((1 - 3 * b + 3 * c) * a + (3 * b - 6 * c)) * a + 3 * c) * a
        }
        var f = "Float32Array" in k;
        if (4 !== arguments.length) return !1;
        for (var e = 0; 4 > e; ++e)
            if ("number" !== typeof arguments[e] || isNaN(arguments[e]) || !isFinite(arguments[e])) return !1;
        b = Math.min(b, 1);
        a = Math.min(a, 1);
        b = Math.max(b, 0);
        a = Math.max(a, 0);
        var p = f ? new Float32Array(11) : Array(11),
            m = !1;
        f = function (f) {
            if (!m && (m = !0, b !== c || a !== d))
                for (var e = 0; 11 > e; ++e) p[e] = g(.1 * e, b, a);
            if (b === c && a === d) return f;
            if (0 === f) return 0;
            if (1 === f) return 1;
            var h = 0;
            for (e = 1; 10 !== e && p[e] <= f; ++e) h += .1;
            --e;
            e = h + (f - p[e]) / (p[e + 1] - p[e]) * .1;
            var l = 3 * (1 - 3 * a + 3 * b) * e * e + 2 * (3 * a - 6 * b) * e + 3 * b;
            if (.001 <= l) {
                for (h = 0; 4 > h; ++h) {
                    l = 3 * (1 - 3 * a + 3 * b) * e * e + 2 * (3 * a - 6 * b) * e + 3 * b;
                    if (0 === l) break;
                    var k = g(e, b, a) - f;
                    e -= k / l
                }
                f = e
            } else if (0 === l) f = e;
            else {
                e = h;
                h += .1;
                var Q = 0;
                do k = e + (h - e) / 2, l = g(k, b, a) - f, 0 < l ? h = k : e = k; while (1E-7 < Math.abs(l) && 10 > ++Q);
                f = k
            }
            return g(f, c, d)
        };
        f.getControlPoints = function () {
            return [{
                x: b,
                y: c
            }, {
                x: a,
                y: d
            }]
        };
        var h = "generateBezier(" + [b, c, a, d] + ")";
        f.toString = function () {
            return h
        };
        return f
    }

    function R(b) {
        return function (c) {
            return 1 / b *
                Math.round(c * b)
        }
    }

    function S(b) {
        function c(b) {
            b.forEach(function (b) {
                switch (b[0]) {
                    case 0:
                        a.push(b[1], b[2], " ");
                        break;
                    case 1:
                        a.push(b[1], "(");
                        b[2].forEach(function (b, d) {
                            0 !== d && a.push(",");
                            c(b)
                        });
                        a.push(")", " ");
                        break;
                    case 2:
                        a.push("rgb(");
                        b[1].forEach(function (b, c) {
                            0 !== c && a.push(",");
                            a.push(b)
                        });
                        a.push(")", " ");
                        break;
                    case 3:
                        a.push(b[1], " ")
                }
            })
        }
        var a = [];
        c(b);
        a.pop();
        return a.join("")
    }

    function z(b, c, a) {
        E[c.nameJS] ? (c = E[c.nameJS](b, c, a)) && (b.style[c[0]] = c[1]) : b.style[c.nameJS] = a
    }

    function T(b) {
        return b.replace(/[A-Z]/g,
            function (b) {
                return "-" + b.toLowerCase()
            })
    }

    function U(b) {
        return b.split("-").map(function (b, a) {
            return 0 !== a ? b.charAt(0).toUpperCase() + b.slice(1) : b
        }).join("")
    }

    function A(b, c, a) {
        switch (a) {
            case "+":
                return b + c;
            case "-":
                return b - c;
            case "*":
                return b * c;
            case "/":
                return b / c;
            case "^":
                return Math.pow(b, c);
            default:
                return b
        }
    }

    function F(b, c, a) {
        for (var d = 0; d < c.length; ++d) {
            if (!b[d]) throw "Fail";
            switch (c[d][0]) {
                case 0:
                    !1 === c[d][3] && (c[d][1] = 0);
                    c[d][1] = A(b[d][1], c[d][1], a);
                    break;
                case 1:
                    c[d][2].forEach(function (g, f) {
                        F(b[d][2][f],
                            c[d][2][f], a)
                    });
                    break;
                case 2:
                    !1 === c[d][2] ? c[d][1].forEach(function (b, f) {
                        c[d][1][f] = parseInt(A(0, b, a))
                    }) : c[d][1].forEach(function (g, f) {
                        c[d][1][f] = parseInt(A(b[d][1][f], g, a))
                    })
            }
        }
    }

    function G(b, c, a) {
        for (var d = 0; d < a.length; ++d)
            if (c[d] && c[d][0] === a[d][0]) switch (a[d][0]) {
                case 0:
                    a[d][2] && (c[d][2] ? c[d][2] !== a[d][2] && ("%" === a[d][2] ? (a[d][2] = c[d][2], a[d][1] = a[d][1] / 100 * c[d][1]) : (c[d][1] = V(b, c[d][1], c[d][2], a[d][2]), c[d][2] = a[d][2])) : c[d][2] = a[d][2]);
                    c[d][2] = a[d][2];
                    break;
                case 1:
                    a[d][2].forEach(function (g, f) {
                        c[d][2][f] ||
                            (c[d][2][f] = []);
                        G(b, c[d][2][f], a[d][2][f])
                    })
            } else c[d] = a[d].slice(0), c[d].push(!1)
    }

    function H(b, c) {
        for (var a = 0; a < c.length; ++a) {
            if (!b[a]) throw "Fail";
            switch (c[a][0]) {
                case 0:
                    b[a][3] = c[a][1] - b[a][1];
                    break;
                case 2:
                    b[a][2] = [];
                    c[a][1].forEach(function (c, g) {
                        b[a][2][g] = c - b[a][1][g]
                    });
                    break;
                case 1:
                    c[a][2].forEach(function (d, g) {
                        H(b[a][2][g], c[a][2][g])
                    });
                    break;
                case 3:
                    b[a][1] = c[a][1]
            }
        }
    }

    function W(b, c, a) {
        function d(b) {
            b.forEach(function (b) {
                switch (b[0]) {
                    case 0:
                        g.push(b[1] + b[3] * a, b[2], " ");
                        break;
                    case 1:
                        g.push(b[1],
                            "(");
                        b[2].forEach(function (a, b) {
                            0 !== b && g.push(",");
                            d(a)
                        });
                        g.push(")", " ");
                        break;
                    case 2:
                        g.push("rgb(");
                        b[1].forEach(function (c, d) {
                            0 !== d && g.push(",");
                            g.push(Math.floor(c + b[2][d] * a))
                        });
                        g.push(")", " ");
                        break;
                    case 3:
                        g.push(b[1], " ")
                }
            })
        }
        var g = [];
        d(c.originalValue);
        g.pop();
        z(b.element, c, g.join(""))
    }

    function I(b) {
        b = b.split("");
        var c = [],
            a = [],
            d, g = b.length,
            f = 0;
        for (d = 0; d < g; ++d) {
            var e = b[d];
            c.push(e);
            if ('"' === e || "'" === e) {
                var p = e,
                    m = !1;
                for (++d; d < g; d++)
                    if (e = b[d], c.push(e), "\\" === e) m = !0;
                    else if (e !== p || m) m && (m = !1);
                else break
            } else if ("(" === e) f++;
            else if (")" === e) {
                if (f--, 0 > f) throw "Fail";
            } else 0 === f && "," === e && (c.pop(), a.push(c.join("")), c = [])
        }
        c.length && a.push(c.join(""));
        return a
    }

    function n(b, c) {
        c || (c = []);
        if (!b) return c;
        if ('"' === b.charAt(0)) {
            var a = b.match(/("(?:[^"\\]|\\.)*")(?: (.*))?/);
            c.push([3, a[1]]);
            n(a[2], c)
        } else if ("'" === b.charAt(0)) a = b.match(/('(?:[^'\\]|\\.)*')(?: (.*))?/), c.push([3, a[1]]), n(a[2], c);
        else if (a = b.match(/^(\-?[0-9\.]*)(em|ex|%|px|cm|mm|in|pt|pc|ch|rem|vh|vw|vmin|vmax|s|ms|deg|grad|rad|turn|Q)?(?: (.*))?/),
            a[1]) c.push([0, parseFloat(a[1]), a[2] || ""]), n(a[3], c);
        else if ((a = b.match(/^([a-z\-]*?)\(([^\)]*)\)(?: (.*))?/)) && a[1]) {
            if ("rgb" === a[1]) c.push([2, I(a[2]).map(function (a) {
                return parseInt(a)
            })]);
            else {
                var d = I(a[2]).map(function (a) {
                    return n(a.trim())
                });
                c.push([1, a[1], d])
            }
            n(a[3], c)
        } else "#" === b.charAt(0) ? (a = b.match(/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})(?: (.*))?/), c.push([2, P(a[1])]), n(a[2], c)) : (a = b.match(/^([a-z\-]*?)(?: (.*))/)) && a[1] ? (q[a[1]] ? c.push([2, q[a[1]].slice(0)]) : c.push([3, a[1]]), n(a[2], c)) : q[b] ?
            c.push([2, q[b].slice(0)]) : c.push([3, b]);
        return c
    }

    function J(b, c, a) {
        var d = {},
            g;
        for (g in c)(function (c, g) {
            var e = c[g],
                f = a.easing;
            "object" === typeof e && (f = e.easing || a.easing, e = e.value);
            e = e.toString();
            var h = !1;
            "=" === e.charAt(1) && (h = e.charAt(0), e = e.substr(2));
            var l = {
                nameJS: U(g),
                nameCSS: T(g),
                toValue: null,
                toValueRaw: null,
                originalValue: null,
                originalValueRaw: null,
                init: function () {
                    var a = K[this.nameJS] ? K[this.nameJS](b, this) : b.style[this.nameJS] ? b.style[this.nameJS] : k.getComputedStyle(b).getPropertyValue(this.nameCSS);
                    this.originalValueRaw = a;
                    if (!this.originalValueRaw || this.originalValueRaw === this.toValueRaw) return z(b, this, this.toValueRaw), !1;
                    this.originalValue = n(this.originalValueRaw);
                    G(b, this.originalValue, this.toValue);
                    h && F(this.originalValue, this.toValue, h);
                    H(this.originalValue, this.toValue);
                    return !0
                }
            };
            l.toValueRaw = e;
            l.toValue = n(l.toValueRaw);
            d[f] || (d[f] = []);
            a.queue ? d[f].push(l) : l.init() && d[f].push(l)
        })(c, g);
        c = {
            element: b,
            options: a,
            properties: d,
            duration: a.duration,
            init: function () {
                var a = !1,
                    b;
                for (b in d) d[b] =
                    d[b].filter(function (b) {
                        return b.init() ? a = !0 : !1
                    });
                return a
            }
        };
        a.queue || a.start();
        h[a.queue ? "main" : "parrallel"].list.splice(0, 0, c);
        x || (u = !1, B(!1))
    }

    function L(b, c) {
        var a = c / b.duration,
            d;
        for (d in b.properties) v[d](a), b.properties[d].forEach(function (c) {
            W(b, c, a)
        })
    }

    function M(b, c) {
        for (var a in b.properties) b.properties[a].forEach(function (a) {
            z(b.element, a, S(a.toValue))
        });
        a = h[c];
        if (a.parrallel) {
            var d = a.list.indexOf(b);
            a.list.splice(d, 1)
        } else a.active = !1;
        b.options.done()
    }

    function B(b) {
        if (u) return x = !1;
        x = !0;
        k.requestAnimationFrame(function () {
            B(!1)
        });
        w = N.now();
        X()
    }

    function X() {
        var b = !0,
            c;
        for (c in h)
            if (h[c].parrallel) h[c].list.forEach(function (a) {
                if (void 0 === a.startTime) a.startTime = w;
                else {
                    var d = w - a.startTime;
                    d >= a.duration ? M(a, c) : L(a, d, c)
                }
                b = !1
            });
            else {
                !h[c].active && h[c].list.length && (h[c].active = h[c].list.pop(), h[c].active.options.start(), h[c].active.init() || (h[c].active.done(), h[c].active = !1), b = !1);
                var a = h[c].active;
                if (a) {
                    if (void 0 === a.startTime) a.startTime = w;
                    else {
                        var d = w - a.startTime;
                        d >= a.duration ? M(a,
                            c) : L(a, d, c)
                    }
                    b = !1
                }
            }
        b && (u = b)
    }

    function r(b, c, a, d, g) {
        if ("string" === typeof b) return r[b].apply(Array.from(arguments).slice(1));
        a && "object" !== typeof a && ("function" === typeof a ? (a = {
            done: a
        }, d && (a.duration = d), g && (a.easing = g)) : "number" === typeof a && (a = {
            duration: a
        }, d && (a.easing = d), g && (a.done = g)));
        a = C(a || {});
        if (Array.isArray(c)) {
            var f = function () {
                    var d = c[m];
                    if (d) {
                        var g = C(a);
                        e && 0 === m && (g.start = e);
                        g.done = function () {
                            m++;
                            m === c.length ? h() : f()
                        };
                        J(b, d, g)
                    }
                },
                e = !1,
                h = !1;
            a.start && (e = a.start, a.start = function () {});
            a.done && (h =
                a.done, a.done = function () {});
            var m = 0;
            f()
        } else return J(b, c, a)
    }
    var v = {},
        h = {
            main: {
                list: [],
                active: !1,
                parrallel: !1
            },
            parrallel: {
                list: [],
                active: !1,
                parrallel: !0
            }
        },
        u = !0,
        x = !1,
        E = {},
        K = {},
        q = {
            aliceblue: "240,248,255",
            antiquewhite: "250,235,215",
            aquamarine: "127,255,212",
            aqua: "0,255,255",
            azure: "240,255,255",
            beige: "245,245,220",
            bisque: "255,228,196",
            black: "0,0,0",
            blanchedalmond: "255,235,205",
            blueviolet: "138,43,226",
            blue: "0,0,255",
            brown: "165,42,42",
            burlywood: "222,184,135",
            cadetblue: "95,158,160",
            chartreuse: "127,255,0",
            chocolate: "210,105,30",
            coral: "255,127,80",
            cornflowerblue: "100,149,237",
            cornsilk: "255,248,220",
            crimson: "220,20,60",
            cyan: "0,255,255",
            darkblue: "0,0,139",
            darkcyan: "0,139,139",
            darkgoldenrod: "184,134,11",
            darkgray: "169,169,169",
            darkgrey: "169,169,169",
            darkgreen: "0,100,0",
            darkkhaki: "189,183,107",
            darkmagenta: "139,0,139",
            darkolivegreen: "85,107,47",
            darkorange: "255,140,0",
            darkorchid: "153,50,204",
            darkred: "139,0,0",
            darksalmon: "233,150,122",
            darkseagreen: "143,188,143",
            darkslateblue: "72,61,139",
            darkslategray: "47,79,79",
            darkturquoise: "0,206,209",
            darkviolet: "148,0,211",
            deeppink: "255,20,147",
            deepskyblue: "0,191,255",
            dimgray: "105,105,105",
            dimgrey: "105,105,105",
            dodgerblue: "30,144,255",
            firebrick: "178,34,34",
            floralwhite: "255,250,240",
            forestgreen: "34,139,34",
            fuchsia: "255,0,255",
            gainsboro: "220,220,220",
            ghostwhite: "248,248,255",
            gold: "255,215,0",
            goldenrod: "218,165,32",
            gray: "128,128,128",
            grey: "128,128,128",
            greenyellow: "173,255,47",
            green: "0,128,0",
            honeydew: "240,255,240",
            hotpink: "255,105,180",
            indianred: "205,92,92",
            indigo: "75,0,130",
            ivory: "255,255,240",
            khaki: "240,230,140",
            lavenderblush: "255,240,245",
            lavender: "230,230,250",
            lawngreen: "124,252,0",
            lemonchiffon: "255,250,205",
            lightblue: "173,216,230",
            lightcoral: "240,128,128",
            lightcyan: "224,255,255",
            lightgoldenrodyellow: "250,250,210",
            lightgray: "211,211,211",
            lightgrey: "211,211,211",
            lightgreen: "144,238,144",
            lightpink: "255,182,193",
            lightsalmon: "255,160,122",
            lightseagreen: "32,178,170",
            lightskyblue: "135,206,250",
            lightslategray: "119,136,153",
            lightsteelblue: "176,196,222",
            lightyellow: "255,255,224",
            limegreen: "50,205,50",
            lime: "0,255,0",
            linen: "250,240,230",
            magenta: "255,0,255",
            maroon: "128,0,0",
            mediumaquamarine: "102,205,170",
            mediumblue: "0,0,205",
            mediumorchid: "186,85,211",
            mediumpurple: "147,112,219",
            mediumseagreen: "60,179,113",
            mediumslateblue: "123,104,238",
            mediumspringgreen: "0,250,154",
            mediumturquoise: "72,209,204",
            mediumvioletred: "199,21,133",
            midnightblue: "25,25,112",
            mintcream: "245,255,250",
            mistyrose: "255,228,225",
            moccasin: "255,228,181",
            navajowhite: "255,222,173",
            navy: "0,0,128",
            oldlace: "253,245,230",
            olivedrab: "107,142,35",
            olive: "128,128,0",
            orangered: "255,69,0",
            orange: "255,165,0",
            orchid: "218,112,214",
            palegoldenrod: "238,232,170",
            palegreen: "152,251,152",
            paleturquoise: "175,238,238",
            palevioletred: "219,112,147",
            papayawhip: "255,239,213",
            peachpuff: "255,218,185",
            peru: "205,133,63",
            pink: "255,192,203",
            plum: "221,160,221",
            powderblue: "176,224,230",
            purple: "128,0,128",
            red: "255,0,0",
            rosybrown: "188,143,143",
            royalblue: "65,105,225",
            saddlebrown: "139,69,19",
            salmon: "250,128,114",
            sandybrown: "244,164,96",
            seagreen: "46,139,87",
            seashell: "255,245,238",
            sienna: "160,82,45",
            silver: "192,192,192",
            skyblue: "135,206,235",
            slateblue: "106,90,205",
            slategray: "112,128,144",
            snow: "255,250,250",
            springgreen: "0,255,127",
            steelblue: "70,130,180",
            tan: "210,180,140",
            teal: "0,128,128",
            thistle: "216,191,216",
            tomato: "255,99,71",
            turquoise: "64,224,208",
            violet: "238,130,238",
            wheat: "245,222,179",
            whitesmoke: "245,245,245",
            white: "255,255,255",
            yellowgreen: "154,205,50",
            yellow: "255,255,0"
        },
        t;
    for (t in q) q[t] = q[t].split(",").map(function (b) {
        return parseInt(b)
    });
    k.requestAnimationFrame ||
        (k.requestAnimationFrame = k.webkitRequestAnimationFrame || k.mozRequestAnimationFrame);
    var N = k.performance && k.performance.now ? k.performance : Date;
    var w = N.now(),
        O = {
            ease: [.25, .1, .25, 1],
            "ease-in": [.42, 0, 1, 1],
            "ease-out": [0, 0, .58, 1],
            "ease-in-out": [.42, 0, .58, 1],
            easeInSine: [.47, 0, .745, .715],
            easeOutSine: [.39, .575, .565, 1],
            easeInOutSine: [.445, .05, .55, .95],
            easeInQuad: [.55, .085, .68, .53],
            easeOutQuad: [.25, .46, .45, .94],
            easeInOutQuad: [.455, .03, .515, .955],
            easeInCubic: [.55, .055, .675, .19],
            easeOutCubic: [.215, .61, .355, 1],
            easeInOutCubic: [.645, .045, .355, 1],
            easeInQuart: [.895, .03, .685, .22],
            easeOutQuart: [.165, .84, .44, 1],
            easeInOutQuart: [.77, 0, .175, 1],
            easeInQuint: [.755, .05, .855, .06],
            easeOutQuint: [.23, 1, .32, 1],
            easeInOutQuint: [.86, 0, .07, 1],
            easeInExpo: [.95, .05, .795, .035],
            easeOutExpo: [.19, 1, .22, 1],
            easeInOutExpo: [1, 0, 0, 1],
            easeInCirc: [.6, .04, .98, .335],
            easeOutCirc: [.075, .82, .165, 1],
            easeInOutCirc: [.785, .135, .15, .86]
        };
    for (t in O) {
        var y = O[t];
        v[t] = D(y[0], y[1], y[2], y[3])
    }
    v.linear = function (b) {
        return b
    };
    v.swing = function (b) {
        return .5 - Math.cos(b *
            Math.PI) / 2
    };
    var Y = function () {
            function b(a, b, c) {
                var d = a.v + c.dv * b;
                return {
                    dx: d,
                    dv: -a.tension * (a.x + c.dx * b) - a.friction * d
                }
            }

            function c(a, c) {
                var d = {
                        dx: a.v,
                        dv: -a.tension * a.x - a.friction * a.v
                    },
                    f = b(a, .5 * c, d),
                    e = b(a, .5 * c, f),
                    h = b(a, c, e),
                    m = 1 / 6 * (d.dv + 2 * (f.dv + e.dv) + h.dv);
                a.x += 1 / 6 * (d.dx + 2 * (f.dx + e.dx) + h.dx) * c;
                a.v += m * c;
                return a
            }
            return function e(b, g, f) {
                var d = {
                        x: -1,
                        v: 0,
                        tension: null,
                        friction: null
                    },
                    h = [0],
                    k = 0,
                    l;
                b = parseFloat(b) || 500;
                g = parseFloat(g) || 20;
                f = f || null;
                d.tension = b;
                d.friction = g;
                (l = null !== f) ? (k = e(b, g), b = k / f * .016) : b =
                    .016;
                for (;;) {
                    var n = c(n || d, b);
                    h.push(1 + n.x);
                    k += 16;
                    if (!(1E-4 < Math.abs(n.x) && 1E-4 < Math.abs(n.v))) break
                }
                return l ? function (b) {
                    return h[b * (h.length - 1) | 0]
                } : k
            }
        }(),
        V = function () {
            function b() {
                document.body.appendChild(a);
                "px in cm mm pt pc".split(" ").forEach(function (b) {
                    a.style.width = 150 + b;
                    c[b] = 150 / a.offsetWidth
                });
                document.body.removeChild(a)
            }
            var c = {},
                a = document.createElement("div");
            a.style.visibility = a.style.overflow = "hidden";
            k.addEventListener("load", function () {
                b()
            });
            return function (b, g, f, e) {
                var d = c[f],
                    h =
                    c[e];
                d && h || (b.appendChild(a), d || (a.style.width = 150 + f, d = 150 / a.offsetWidth), h || (a.style.width = 150 + e, h = 150 / a.offsetWidth), b.removeChild(a));
                return g / d * h
            }
        }();
    r.addEase = function (b, c) {
        v[b] = "function" === typeof c ? c : "object" === typeof c ? 4 === c.length ? D(c[0], c[1], c[2], c[3]) : Y(c[0], c[1]) : R(c)
    };
    r.stop = function () {
        u = !0
    };
    r.start = function () {
        x || (u = !1, B(!1))
    };
    r.clear = function () {
        h = {
            main: {
                list: [],
                active: !1,
                parrallel: !1
            },
            parrallel: {
                list: [],
                active: !1,
                parrallel: !0
            }
        }
    };
    HTMLElement.prototype.D = function (b, c, a, d) {
        return r(this,
            b, c, a, d)
    };
    return r
}(window);
