chrome.contextMenus.create({
    title: "Edit element",
    contexts: ["all"],
    onclick: function (info, tab) {
        console.log('clicked')
        chrome.tabs.sendMessage(tab.id, {
            type: "context_clicked"
        }, {
            frameId: info.frameId
        })
    }
});

chrome.browserAction.onClicked.addListener(function (tab) {
    console.log("Clicked");
    chrome.tabs.sendMessage(tab.id, {
        type: "button_clicked"
    });
});

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse){
    console.log(request);
    
    switch(request.type){
        case 'reload':
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                let id = tabs[0].id;
                chrome.tabs.reload(id);
                console.log('reloaded tab');
            });
            break;
    }
});
