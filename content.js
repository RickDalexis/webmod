window.onload = function () {
    let url = cutUrl(window.location.href);
    //chrome.storage.local.clear();
    chrome.storage.local.get({
        styleData: "{}"
    }, (obj) => {
        obj = JSON.parse(obj.styleData);
        if (!obj || !obj[url]) return;
        console.info('---------------------------retrieved storage----------------------------');
        loadInterface(obj[url]);
    });
}

// creates listener for background query 
let createdInterface = false;

//?? one registered background click leads to 5 responses.
chrome.runtime.onMessage.addListener(
    function (message, sender, sendResponse) {
        //console.log(message);
        switch (message.type) {
            case "button_clicked":
                if (!createdInterface) {
                    //console.log('creating interface');
                    createdInterface = true;
                    createInterface();
                }
                break;
        }
    });



var originalEl = false;
var lastSelected = false;
var mainOverlay = false;

var ignoreHover = false;

let idCounter = 0;
let overlayElements = [];
let elementsChanged = [];

function dragResize(el, overlay) {
    //add changed elements to a list for easy saving
    if (elementsChanged.indexOf(el) === -1) elementsChanged.push(el);

    var styles = window.getComputedStyle(el)
    el.style.overflow = "auto"


    if (styles.position !== "relative" && styles.position !== "absolute" && styles.position !== "fixed") {
        el.style.position = "relative"

    }
    var w = parseInt(overlay.style.width),
        h = parseInt(overlay.style.height),
        x = overlay.offsetLeft,
        y = overlay.offsetTop;

    var oy = parseInt(styles.top) - y,
        ox = parseInt(styles.left) - x;

    var x2 = getOffsetLeft(el),
        y2 = getOffsetTop(el)


    var container = overlay
    container.style.setProperty('pointer-events', 'all', 'important')


    var top = document.createElement('div');
    top.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; height: 5px; cursor: row-resize; left: 5px; right: 5px; top: 0px"
    container.appendChild(top)

    var bottom = document.createElement('div');
    bottom.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; height: 5px; cursor: row-resize; left: 5px; right: 5px; bottom: 0px"
    container.appendChild(bottom)

    var left = document.createElement('div');
    left.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; cursor: col-resize; top: 5px; left: 0px; bottom: 5px"
    container.appendChild(left)

    var right = document.createElement('div');
    right.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; cursor: col-resize; top: 5px; right: 0px; bottom: 5px"
    container.appendChild(right)

    var corner1 = document.createElement('div');
    corner1.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; height: 5px; cursor: nwse-resize; bottom: 0px; right: 0px;"
    container.appendChild(corner1)

    corner1.addEventListener('mousedown', (e) => {
        horizontal(e);
        vertical(e);
        e.stopPropagation();
    })

    var corner2 = document.createElement('div');
    corner2.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; height: 5px; cursor: nwse-resize; top: 0px; left: 0px;"
    container.appendChild(corner2)

    corner2.addEventListener('mousedown', (e) => {
        horizontal(e, true);
        vertical(e, true);
        e.stopPropagation();
    })

    var corner3 = document.createElement('div');
    corner3.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; height: 5px; cursor: nesw-resize; top: 0px; right: 0px;"
    container.appendChild(corner3)

    corner3.addEventListener('mousedown', (e) => {
        horizontal(e);
        vertical(e, true);
        e.stopPropagation();
    })


    var corner4 = document.createElement('div');
    corner4.style = "background-color: rgba(0,0,0,.3); z-index: 100000; position: absolute; width: 5px; height: 5px; cursor: nesw-resize; bottom: 0px; left: 0px;"
    container.appendChild(corner4)

    corner4.addEventListener('mousedown', (e) => {
        horizontal(e, true);
        vertical(e);
        e.stopPropagation();
    })


    var center = container;
    center.style.backgroundColor = "rgba(0,0,0,.2)"
    center.style.cursor = "all-scroll"
    // container.appendChild(center)


    top.addEventListener('mousedown', (e) => {
        vertical(e, true);
        e.stopPropagation();
    })

    left.addEventListener('mousedown', (e) => {
        horizontal(e, true);
        e.stopPropagation();
    })
    bottom.addEventListener('mousedown', (e) => {
        vertical(e);
        e.stopPropagation();
    })
    right.addEventListener('mousedown', (e) => {
        horizontal(e);
        e.stopPropagation();
    })

    var movingVert = false;
    var movingHor = false;
    var moving = false;
    var startY = 0;
    var startX = 0;
    var centerEvent = (e) => {
        moving = true;
        ignoreHover = true;
        updateDim(e);
    }
    center.addEventListener('mousedown', centerEvent)

    function updateDim(e) {
        startX = e.clientX;
        startY = e.clientY;
        x = overlay.offsetLeft
        y = overlay.offsetTop
        h = parseInt(overlay.style.height)
        w = parseInt(overlay.style.width)
    }

    function vertical(e, l) {
        ignoreHover = true;
        movingVert = l ? 2 : 1;
        updateDim(e)
    }

    function horizontal(e, l) {
        ignoreHover = true;
        movingHor = l ? 2 : 1;
        updateDim(e);
    }

    function mouseup(e) {

        //   ignoreHover = false;
        movingVert = false;
        movingHor = false;
        moving = false;
        e.stopPropagation();
    }

    function update() {
        el.style.width = overlay.style.width;
        el.style.height = overlay.style.height;

        el.style.top = (oy + container.offsetTop) + 'px';
        el.style.left = (ox + container.offsetLeft) + 'px';



        var current = el;

        var x = current.offsetLeft;
        var y = current.offsetTop;
        var width = current.offsetWidth;
        var height = current.offsetHeight;

        while (current.parentElement) {
            var parent = current.parentElement;


            if (current.offsetParent != parent) {

            } else {
                x += parent.offsetLeft;
                y += parent.offsetTop;
            }
            var styles = getComputedStyles(parent)
            if (x < parent.offsetLeft ||
                x + width > parent.offsetLeft + parent.offsetWidth) {
                if (styles.overflowX === "hidden" || styles.overflowX === "auto") {
                    parent.style.overflowX = "visible"
                    console.log("overflowX", current, parent)
                    if (elementsChanged.indexOf(parent) === -1) elementsChanged.push(parent);

                }
            }
            if (y < parent.offsetTop ||
                y + height > parent.offsetTop + parent.offsetHeight) {
                if (styles.overflowY === "hidden" || styles.overflowY === "auto") {
                    parent.style.overflowY = "visible"
                    console.log("overflowY", current, parent)
                    if (elementsChanged.indexOf(parent) === -1) elementsChanged.push(parent);

                }
            }
            current = parent
        }


    }

    function mousemove(e) {
        var dy = e.clientY - startY,
            dx = e.clientX - startX;

        if (movingVert == 1) {
            overlay.style.height = (h + dy) + 'px'
            update()
        } else if (movingVert == 2) {
            overlay.style.height = (h - dy) + 'px'
            overlay.style.top = y + dy + 'px'
            update()
        }
        if (movingHor == 1) {
            overlay.style.width = (w + dx) + 'px'
            update()
        } else if (movingHor == 2) {
            overlay.style.width = (w - dx) + 'px'
            overlay.style.left = x + dx + 'px'
            update();
        }
        if (moving == true) {
            overlay.style.top = y + dy + 'px'
            overlay.style.left = x + dx + 'px'
            update();
        }
    }
    window.addEventListener('mouseup', mouseup)
    container.addEventListener('mouseup', mouseup)
    window.addEventListener('mousemove', mousemove)
    return {
        remove: function () {
            container.removeChild(top)
            container.removeChild(bottom)
            container.removeChild(left)
            container.removeChild(right)
            container.removeChild(corner1)
            container.removeChild(corner2)
            container.removeChild(corner3)
            container.removeChild(corner4)
            //container.removeChild(center)
            center.style.backgroundColor = "";
            ignoreHover = false;
            center.removeEventListener("mousedown", centerEvent)
            window.removeEventListener('mouseup', mouseup)
            container.removeEventListener('mouseup', mouseup)
            window.removeEventListener('mousemove', mousemove)
        },
        element: el,
        container: container
    }
}

function getOverlayID() {
    return idCounter++;
}

//pass in stored value with url as key
function loadInterface(stored) {
    console.log('loadInterface');
    console.log(stored);

    stored.forEach((item) => {
        var element = document.querySelector(item[0]);
        if (element) {
            console.log("Element found")
            for (var property in item[1]) {
                if (item[1][property]) {
                    element.style[property] = item[1][property];
                }
            }
        } else {
            console.log("Element not found")
        }
        console.log(item)
    })

}

function createInterface() {

    let overlayZBase = 10000000; //overlay equivalent of 0 z

    var overlay = document.createElement('div');
    overlay.style = "position: absolute; bottom: 0px; top: 100px; left: 0px; right: 0px; z-index: " + overlayZBase + ";";
    overlay.classList.add('webmod', 'overlay');

    mainOverlay = overlay;

    //create an overlay with corresbonding z-index for every div
    for (var i = 0; i < document.body.children.length; i++) {
        var ret = createDivOverlay(document.body.children[i]);
        if (ret) overlay.appendChild(ret);
    }

    document.body.style.position = "initial"
    document.body.style.marginTop = (parseInt(getComputedStyles(document.body).marginTop) + 100) + "px";

    moveDownFixed(document.body);

    document.body.appendChild(overlay);

    var interface = document.createElement('div');


    //this is the style for the top control interface 
    interface.style = "background-color: rgb(230,230,230); position: fixed; top: 0px; left: 0px; right: 0px; height: 0px; z-index: 0";
    interface.classList.add('webmod', 'interface');
    overlay.appendChild(interface);
    interface.style.height = "100px"

    //creation of the buttons that are in the interface

    //this is the save button
    var btn = document.createElement("div");
    btn.style = "cursor: pointer; margin: 5px 9px; font-family: Arial; font-size: 15px; background-color: rgb(44, 221, 103); display: inline-block; padding: 5px 10px; border-radius: 5px; border: 1px solid rgba(0,0,0,.1); font-weight: bold";
    //  text for save button 
    var t = document.createTextNode("Save");
    btn.appendChild(t);
    //ask for url from backgroud once save
    btn.addEventListener('click', function () {
        saveInterface(cutUrl(window.location.href));
        createdInterface = false;
    });
    //actually append save button to interface 
    interface.append(btn);


    // next is the creation of the  reset button
    
    //reset button 

    var reset_btn = document.createElement("div");
    reset_btn.style =  "cursor: pointer; margin: 5px 11px; font-family: Arial; font-size: 15px; background-color: rgb(255, 000, 000); display: inline-block; padding: 5px 10px; border-radius: 5px; border: 1px solid rgba(0,0,0,.1); font-weight: bold";
    var t3 = document.createTextNode("Reset");
    reset_btn.append(t3);
    interface.append(reset_btn);
    reset_btn.addEventListener('click', resetPage);

    



} //createinterface


//delete all overlays and save configs to storage
function saveInterface(url) {
    //if(!chrome.storage.local.get('webmod')) chrome.storage.sync.set({webmod: {}}, () => {console.log('webmod storage space created')});

    console.log('saving: url: ' + url);
    chrome.storage.local.get({
        styleData: "{}"
    }, (obj) => {


        obj[url] = [];

        for (let i = 0, l = elementsChanged.length; i < l; i++) {
            let identifier = getSingleSelector(elementsChanged[i]);
            var style = elementsChanged[i].style
            var styleDt = {
                width: style.width,
                height: style.height,
                position: style.position,
                left: style.left,
                top: style.top,
                overflowX: style.overflowX,
                overflowY: style.overflowY
            }
            obj[url].push([identifier, styleDt]);

            //console.log('elementStyle: ' + elementsChanged[i].style);
        }
        chrome.storage.local.set({
            styleData: JSON.stringify(obj)
        }, () => {
            console.log('finished saving: ');
            console.log(obj);
        });

        //delete all editing related stuff
        let overlays = document.querySelectorAll('.webmod');
        overlays.forEach((overlay) => {
            overlay.parentElement.removeChild(overlay);
        });

        //reverse the top offset
        document.body.style.marginTop = (parseInt(getComputedStyles(document.body).marginTop) - 100) + "px"
        moveUpFixed(document.body);
    })
}

function createDivOverlay(element, level) {
    level = level || 0;

    // Do not process scripts
    if (element.tagName === "script") return false;

    // Create the overlay
    let overlay = document.createElement('div');
    let styles = getComputedStyles(element); // get the computed styles

    // Check for visibility
    if (styles.visibility == "hidden" || styles.display == "none" || element.offsetWidth == 0 || element.offsetHeight == 0) {
        return false;
    }


    //so overlays can reference their elements
    overlay.dataset.index = getOverlayID();
    overlayElements[overlay.dataset.index] = element;

    overlay.classList.add('webmod-div-overlay');

    overlay.style.position = "absolute";
    overlay.style.width = element.offsetWidth + "px";
    overlay.style.height = element.offsetHeight + "px";
    var y = element.offsetTop,
        x = element.offsetLeft;

    // Because absolute positioning makes the childs positioning relative to it, the difference must be used
    if (element.parentElement && element.offsetParent != element.parentElement) {
        x = x - element.parentElement.offsetLeft;
        y = y - element.parentElement.offsetTop;
    }

    x -= 4;
    y -= 4;
    overlay.style.top = y + "px";
    overlay.style.left = x + "px";
    overlay.style.zIndex = styles.zIndex;
    //  overlay.dataset.original = getSingleSelector(element)

    var color = getColor(level, 0.7);

    var currentIndex = overlay.style.zIndex;

    var init = false;

    overlay.addEventListener('mouseenter', () => {
        if (ignoreHover || originalEl) return;


        overlay.style.border = "4px solid " + color;
        if (!init) {
            if (overlay.parentElement && overlay.parentElement.classList.contains("webmod-div-overlay")) {
                var w = overlay.parentElement.offsetWidth;
                if (overlay.offsetLeft + overlay.offsetWidth > w - 10) {
                    var d = (overlay.offsetLeft + overlay.offsetWidth) - (w - 10);
                    overlay.style.width = (parseInt(overlay.style.width) - d) + "px"
                }
            }
            init = true;
        }

        // overlay.style.height = (element.offsetHeight + 3) + "px"
    });

    overlay.addEventListener('mouseleave', () => {
        if (ignoreHover || (originalEl && originalEl.element != element)) return
        console.log('leave');

        if (originalEl) originalEl.remove(), originalEl = false;

        overlay.style.border = "";


    });
    overlay.addEventListener('mouseup', (e) => {
        console.log("click");
        if (ignoreHover || (originalEl && originalEl.element == element)) {
            return ignoreHover = false;
        }

        if (originalEl) {
            originalEl.remove();
        }
        if (lastSelected) {
            lastSelected.element.style.zIndex = "1000000";
            lastSelected.container.style.zIndex = "10000000";
        }

        lastSelected = originalEl = dragResize(element, overlay);
        element.style.zIndex = "1000001";
        overlay.style.zIndex = "10000001";
        e.stopPropagation();
    })

    // Recurse
    if (element.children) {

        for (var i = 0; i < element.children.length; i++) {
            var ret = createDivOverlay(element.children[i], level + 1);
            if (ret) overlay.appendChild(ret);
        }
    }

    return overlay;
}//createDivOverlay

//revert layout to the one set by the website owner
function resetPage(url){
    console.log('resetting page');
    chrome.storage.local.get({styleData: '{}'}, (obj) => {

        obj[url] = undefined;

        chrome.storage.local.set({
            styleData: JSON.stringify(obj)
        }, () => {
            console.log('finished reseting: ');
            console.log(obj);

            //tell background to reload page
            chrome.runtime.sendMessage({type: "reload"});
        });
    });
}